#!/bin/sh

set -x
set -e

export PATH=".:$PATH"

# =================================================================
# operator
# =================================================================
manifests() {
  cat keycloak-k8s-resources/kubernetes/keycloaks.k8s.keycloak.org-v1.yml
  echo ---
  cat keycloak-k8s-resources/kubernetes/keycloakrealmimports.k8s.keycloak.org-v1.yml
  echo ---
  cat keycloak-k8s-resources/kubernetes/kubernetes.yml
}

rm -rf keycloak-operator
manifests | helmify -crd-dir -v keycloak-operator

yq ea -i ".description = \"Keycloak Operator\"" keycloak-operator/Chart.yaml
KEYCLOAK_VERSION="$(cat KEYCLOAK_VERSION)"
yq ea -i ".version = \"$KEYCLOAK_VERSION\"" keycloak-operator/Chart.yaml
yq ea -i ".appVersion = \"$KEYCLOAK_VERSION\"" keycloak-operator/Chart.yaml

yq ea -i ".operator.serviceAccount.annotations = {}" keycloak-operator/values.yaml
yq ea -i ".operator.keycloakOperator.env.quarkusOperatorSdkNamespaces = \"keycloak\"" keycloak-operator/values.yaml

patch -p0 < keycloak-operator.diff
helm package keycloak-operator

