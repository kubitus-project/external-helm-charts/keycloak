# Keycloak

This is an Helm chart generated
with [Helmify](https://github.com/arttor/helmify/)
from [keycloak-k8s-resources](https://github.com/keycloak/keycloak-k8s-resources/tree/nightly).

## Usage

```shell
helm repo add kubitus-keycloak https://gitlab.com/api/v4/projects/48807498/packages/helm/stable

helm install kubitus-keycloak/keycloak-operator
```

Available values can be retrieved with:

```shell
helm show values kubitus-keycloak/keycloak-operator
```
