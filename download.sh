#!/bin/sh

set -x
set -e


# Clone keycloak-k8s-resources repo
KEYCLOAK_VERSION="$(cat KEYCLOAK_VERSION)"

rm -rf keycloak-k8s-resources
git clone \
  --quiet \
  --branch "$KEYCLOAK_VERSION" \
  --single-branch \
  --depth 1 \
  https://github.com/keycloak/keycloak-k8s-resources

rm -rf keycloak-k8s-resources-operators-tandem
git clone \
  --quiet \
  --branch operators-tandem \
  --single-branch \
  --depth 1 \
  https://github.com/keycloak/keycloak-k8s-resources \
  keycloak-k8s-resources-operators-tandem

# Download Helmify
HELMIFY_VERSION="$(cat HELMIFY_VERSION)"
curl -fLSs "https://github.com/arttor/helmify/releases/download/$HELMIFY_VERSION/helmify_Linux_x86_64.tar.gz" | tar -xz helmify


# Download YQ
YQ_VERSION="$(cat YQ_VERSION)"
curl -fLSs "https://github.com/mikefarah/yq/releases/download/$YQ_VERSION/yq_linux_amd64.tar.gz" | tar -xz --to-stdout ./yq_linux_amd64 > yq
chmod +x yq

# Download Helm and helm-push
HELM_VERSION="$(cat HELM_VERSION)"
curl -fLSs "https://get.helm.sh/helm-$HELM_VERSION-linux-amd64.tar.gz" | tar -xz --strip-components=1 linux-amd64/helm
./helm plugin uninstall cm-push ||:
./helm plugin install https://github.com/chartmuseum/helm-push.git

